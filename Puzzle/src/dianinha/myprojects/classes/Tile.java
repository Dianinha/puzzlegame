package dianinha.myprojects.classes;

public class Tile {
	private Integer image;
	private Integer place;
	
	public Tile(Integer image, Integer place) {
		super();
		this.image = image;
		this.place = place;
	}
	public Integer getImage() {
		return image;
	}
	public void setImage(Integer image) {
		this.image = image;
	}
	public Integer getPlace() {
		return place;
	}
	public void setPlace(Integer place) {
		this.place = place;
	}
	
	
}
