//package dianinha.myprojects.classes;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javafx.application.Application;
//import javafx.scene.Scene;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.layout.FlowPane;
//import javafx.stage.Stage;
//
//public class StartingScreen extends Application {
//	
//	Image img;
//	int numberOfTiles = 16;
//	
//	public void showMe() {
//		this.launch(null);
//	}
//
//	@Override
//	public void start(Stage primaryStage) throws Exception {
//		primaryStage.setTitle("Uk�adanka");
//		FlowPane rootNode = new FlowPane(10, 10);
//		Scene startingScene = new Scene(rootNode, 900, 900);
//		primaryStage.setScene(startingScene);
//		
//		img = new Image("res/image.jpeg", 600, 600, true, false);
//		ImageView imgView = new ImageView(img);
//		//rootNode.getChildren().add(imgView);
//		ImageGenerator imaG = new ImageGenerator();
//		imaG.createImages(numberOfTiles, img);
//		List<ImageView> tmpList = new ArrayList<>();
//		for (int i = 0; i < numberOfTiles; i++) {
//			tmpList.add(imaG.getTiles().get(i+1));
//		}
//		
//		List <Tile >shuffled = imaG.shuffle(numberOfTiles);
//		List<ImageView> myFinalShuffeledList = new ArrayList<>();
//		
//		for (int i = 0; i < numberOfTiles; i++) {
//			Tile tmp = shuffled.get(i);
//			int whichImage = tmp.getImage();
//			System.out.println(whichImage);
//			myFinalShuffeledList.add(tmpList.get(whichImage-1));
//		}
//		
//		rootNode.getChildren().addAll(myFinalShuffeledList);
//		//rootNode.getChildren().addAll(tmpList);
//		
//		
//		primaryStage.show();
//	}
//
//}
