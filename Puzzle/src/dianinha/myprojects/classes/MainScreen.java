package dianinha.myprojects.classes;

import dianinha.myproject.enums.Difficulty;
import dianinha.myproject.enums.NumTiles;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainScreen extends Application {

	Scene mainScene;
	Label helloLabel, chooseDifficultyLabel, chooseNumberOfTilesLabel, choosePictureLabel;
	Button nine, sixteen, twentyFive, easy, normal, hard, image1, image2, image3, ok;
	Image img1, img2, img3;
	String pathToImage;
	HBox top;
	GridPane center;
	Font myFont = new Font(STYLESHEET_MODENA, 20);

	static Difficulty choosenDiff;
	static NumTiles choosenNumTiles;

	EventHandler<MouseEvent> buttonsTilesHandler;
	EventHandler<MouseEvent> buttonsDiffHandler;
	EventHandler<MouseEvent> buttonsImageHandler;
	EventHandler<MouseEvent> buttonOk;

	Border choosenBorder = new Border(
			new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));

	public void startTheMainScreen() {
		this.launch(null);
	}

	@Override
	public void start(Stage myStage) throws Exception {

		myStage.setTitle("UK�ADANKA");
		myStage.setResizable(false);

		// Setting position on the screen - fixed.
		myStage.setX(50);
		myStage.setY(20);

		mainScene = createAScene();

		buttonOk = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				if (choosenDiff != null && choosenNumTiles != null) {
					SceneWithPuzzle sceneWithChosenSpecs = new SceneWithPuzzle(800, 800, choosenDiff, choosenNumTiles,
							pathToImage);
					sceneWithChosenSpecs.check.setOnAction(new EventHandler<ActionEvent>() {

						@Override
						public void handle(ActionEvent event) {
							if (sceneWithChosenSpecs.checkPuzzle == true) {
								CongratulationsScreen resultsScreen = new CongratulationsScreen(800, 500,
										sceneWithChosenSpecs.numberOfMoves, sceneWithChosenSpecs.choosenDiff,
										sceneWithChosenSpecs.choosenNumTiles);
								myStage.setScene(resultsScreen);

							}
						}
					});

					myStage.setScene(sceneWithChosenSpecs);
					myStage.show();
				}
			}
		};

		ok.setOnMouseClicked(buttonOk);
		myStage.setScene(mainScene);
		myStage.show();

	}

	public Scene createAScene() {
		BorderPane border = new BorderPane();
		border.setPadding(new Insets(30, 15, 30, 15));
		Scene mainScene = new Scene(border, 600, 400);

		// Labels

		helloLabel = new Label("Witaj w uk�adance!");
		helloLabel.setAlignment(Pos.CENTER);
		helloLabel.setFont(new Font(STYLESHEET_MODENA, 30));

		chooseNumberOfTilesLabel = new Label("Wybierz z ilu kafelk�w ma si� sk�ada� uk�adanka:");
		chooseNumberOfTilesLabel.setFont(myFont);

		chooseDifficultyLabel = new Label("Wybierz poziom trudno�ci:");
		chooseDifficultyLabel.setFont(myFont);

		choosePictureLabel = new Label("Wybierz obrazek:");
		choosePictureLabel.setFont(myFont);

		// MouseHandlers

		buttonsTilesHandler = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

				Button choosenButton = (Button) event.getSource();

				if (choosenButton == nine) {
					nine.setBorder(choosenBorder);
					choosenNumTiles = NumTiles.NINE;
					sixteen.setBorder(null);
					twentyFive.setBorder(null);
				} else if (choosenButton == sixteen) {
					sixteen.setBorder(choosenBorder);
					nine.setBorder(null);
					twentyFive.setBorder(null);
					choosenNumTiles = NumTiles.SIXTEEN;
				} else if (choosenButton == twentyFive) {
					twentyFive.setBorder(choosenBorder);
					nine.setBorder(null);
					sixteen.setBorder(null);
					choosenNumTiles = NumTiles.TWENTYFIVE;
				}

			}
		};

		buttonsDiffHandler = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

				Button choosenButton = (Button) event.getSource();

				if (choosenButton == easy) {
					easy.setBorder(choosenBorder);
					choosenDiff = Difficulty.EASY;
					normal.setBorder(null);
					hard.setBorder(null);
				} else if (choosenButton == normal) {
					normal.setBorder(choosenBorder);
					easy.setBorder(null);
					hard.setBorder(null);
					choosenDiff = Difficulty.NORMAL;
				} else if (choosenButton == hard) {
					hard.setBorder(choosenBorder);
					easy.setBorder(null);
					normal.setBorder(null);
					choosenDiff = Difficulty.HARD;
				}

			}
		};

		buttonsImageHandler = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				Button choosenButton = (Button) event.getSource();
				if (choosenButton == image1) {
					image1.setBorder(choosenBorder);
					pathToImage = "res/image.jpeg";
					image2.setBorder(null);
					image3.setBorder(null);
				} else if (choosenButton == image2) {
					image2.setBorder(choosenBorder);
					image1.setBorder(null);
					image3.setBorder(null);
					pathToImage = "res/image2.jpeg";
				} else if (choosenButton == image3) {
					image3.setBorder(choosenBorder);
					image1.setBorder(null);
					image2.setBorder(null);
					pathToImage = "res/image3.jpeg";
				}

			}
		};

		//Buttons
		
		nine = new Button("9");
		nine.setPrefSize(100, 50);
		nine.setOnMouseClicked(buttonsTilesHandler);
		
		sixteen = new Button("16");
		sixteen.setPrefSize(100, 50);
		sixteen.setOnMouseClicked(buttonsTilesHandler);
		
		twentyFive = new Button("25");
		twentyFive.setPrefSize(100, 50);
		twentyFive.setOnMouseClicked(buttonsTilesHandler);
		
		easy = new Button("�ATWY");
		easy.setPrefSize(100, 50);
		easy.setOnMouseClicked(buttonsDiffHandler);
		
		normal = new Button("�REDNI");
		normal.setOnMouseClicked(buttonsDiffHandler);
		normal.setPrefSize(100, 50);
		
		hard = new Button("TRUDNY");
		hard.setPrefSize(100, 50);
		hard.setOnMouseClicked(buttonsDiffHandler);
		
		image1 = new Button();
		img1 = new Image("res/image.jpeg", 45, 45, true, false);
		ImageView imgV1 = new ImageView(img1);
		image1.setGraphic(imgV1);
		image1.setPrefSize(100, 50);
		
		image2 = new Button();
		img2 = new Image("res/image2.jpeg", 45, 45, true, false);
		ImageView imgV2 = new ImageView(img2);
		image2.setGraphic(imgV2);
		image2.setPrefSize(100, 50);
		
		image3 = new Button();
		img3 = new Image("res/image3.jpeg", 45, 45, true, false);
		ImageView imgV3 = new ImageView(img3);
		image3.setGraphic(imgV3);
		image3.setPrefSize(100, 50);

		
		image1.setOnMouseClicked(buttonsImageHandler);
		image2.setOnMouseClicked(buttonsImageHandler);
		image3.setOnMouseClicked(buttonsImageHandler);

		ok = new Button("OK");
		ok.setPrefSize(100, 50);
		ok.setOnMouseClicked(buttonOk);
		
		//Arrangement
		
		top = new HBox();
		top.setAlignment(Pos.CENTER);
		top.getChildren().add(helloLabel);
		border.setTop(top);

		center = new GridPane();
		center.setAlignment(Pos.CENTER);
		center.setPrefSize(500, 500);
		center.setHgap(40);
		center.setVgap(8);
		center.add(chooseNumberOfTilesLabel, 1, 1, 3, 1);
		center.add(nine, 1, 2);
		center.add(sixteen, 2, 2);
		center.add(twentyFive, 3, 2);
		center.add(chooseDifficultyLabel, 1, 3, 3, 1);
		center.add(easy, 1, 4);
		center.add(normal, 2, 4);
		center.add(hard, 3, 4);
		center.add(choosePictureLabel, 1, 5, 3, 1);
		center.add(image1, 1, 7);
		center.add(image2, 2, 7);
		center.add(image3, 3, 7);
		center.add(ok, 2, 8);
		border.setCenter(center);
		
		return mainScene;
	};

}
