package dianinha.myprojects.classes;

import dianinha.myproject.enums.Difficulty;
import dianinha.myproject.enums.NumTiles;

public class Score {
	String name;
	int score;
	Difficulty diff;
	NumTiles numTiles;
	public Score(String name, int score) {
		super();
		this.name = name;
		this.score = score;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "Score [name=" + name + ", score=" + score +"]";
	}
	
	

}
