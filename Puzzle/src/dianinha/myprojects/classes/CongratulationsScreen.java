package dianinha.myprojects.classes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.SchemaOutputResolver;

import dianinha.myproject.enums.Difficulty;
import dianinha.myproject.enums.NumTiles;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class CongratulationsScreen extends Scene {
	static BorderPane border = new BorderPane();
	HBox top;
	Label contratulations, scores;
	Font myFont = new Font(20);
	TextArea name;
	EventHandler<MouseEvent> nameGiven;
	Button ok;
	File records = new File("records/records.txt");
	BufferedWriter existingScores = null;
	List<Score> scoresRecord = new ArrayList<>();
	Difficulty diff;
	NumTiles numTiles;
	GridPane center;

	public int myScoreCounter(int impotedScore) {
		int tmpScoreDiff;
		int tmpScoreNumTiles;

		if (numTiles == NumTiles.NINE) {
			tmpScoreNumTiles = 9;
		} else if (numTiles == NumTiles.SIXTEEN) {
			tmpScoreNumTiles = 16;
		} else if (numTiles == NumTiles.TWENTYFIVE) {
			tmpScoreNumTiles = 25;
		} else
			tmpScoreNumTiles = 0;
		if (diff == Difficulty.EASY) {
			tmpScoreDiff = 10;
		} else if (diff == Difficulty.NORMAL) {
			tmpScoreDiff = 20;
		} else if (diff == Difficulty.HARD) {
			tmpScoreDiff = 30;
		} else
			tmpScoreDiff = 0;
		int score = tmpScoreDiff + tmpScoreNumTiles - impotedScore;
		return score;
	}

	public CongratulationsScreen(int width, int height, int score, Difficulty diff, NumTiles numTiles) {
		super(border, width, height);
		this.diff = diff;
		this.numTiles = numTiles;
		FlowPane rootNode = new FlowPane(10, 10);
		
		top = new HBox();
		border.setTop(top);
		
		
		border.setBottom(rootNode);
		
		
		rootNode.setPadding(new Insets(20));
		scores = new Label("WYNIKI:");

		for (Score oneScore : scoresRecord) {
			scores.setText(scores.getText() + "\n" + oneScore.toString());
		}
		contratulations = new Label("GRATULACJE! TW�J WYNIK TO: " + myScoreCounter(score));
		contratulations.setFont(myFont);
		contratulations.setPrefSize(400, 100);
		name = new TextArea("Wpisz imie");
		name.setPrefHeight(30);
		ok = new Button("OK");

		// to skasowac p�zniej

		//scoresRecord.add(new Score("Diana", 90));

		if (!records.exists()) {
			try {
				records.createNewFile();
			} catch (IOException e) {
				System.out.println("nie uda�o sie");
				e.printStackTrace();
			}
		}
		else {
			try {
				BufferedReader readRecords = new BufferedReader(new FileReader(records));
				while (readRecords.read() != -1) {
					String tmp = readRecords.readLine();
					String addName = tmp.substring(tmp.indexOf("=")+1, tmp.indexOf(","));
					System.out.println(addName);
					String addRecord = tmp.substring(tmp.lastIndexOf("=")+1, tmp.lastIndexOf("]"));
					System.out.println(addRecord);
					scoresRecord.add(new Score(addName, Integer.parseInt(addRecord)));
				}
			} catch (IOException e) {

			}
		}
		;

		try {
			existingScores = new BufferedWriter(new FileWriter(records, true));
			for (Score score2 : scoresRecord) {
				existingScores.write(score2.toString() + "\r\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (existingScores != null) {
				try {
					existingScores.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		
		
		center = new GridPane();
		center.setAlignment(Pos.CENTER);
		border.setCenter(center);
		center.setHgap(40);
		center.setVgap(8);
		center.setPadding(new Insets(0, 0, 40, 0));
		center.add(new Label("IMIE"), 1, 1);
		center.add(new Label("WYNIK"), 2, 1);
		for (int i=0; i<scoresRecord.size();i++) {
			center.add(new Label(scoresRecord.get(i).getName()), 1, i+2);
			center.add(new Label(""+scoresRecord.get(i).getScore()), 2, i+2);
		}
		
		nameGiven = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				Score tmpScore = new Score(name.getText(), score);
				center.add(new Label(tmpScore.getName()), 1, scoresRecord.size()+2);
				center.add(new Label(""+tmpScore.getScore()), 2, scoresRecord.size()+2);
				scoresRecord.add(tmpScore);
				name.setVisible(false);
				ok.setVisible(false);
				BufferedWriter newScore;
				try {
					newScore = new BufferedWriter(new FileWriter(records, true));
					newScore.write(tmpScore.toString() + "\r\n");
					newScore.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

			}
		};
		ok.setOnMouseClicked(nameGiven);
		top.getChildren().add(contratulations);
		top.setAlignment(Pos.CENTER);
		rootNode.getChildren().addAll(name, ok);

	}
}
