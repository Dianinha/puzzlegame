package dianinha.myprojects.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImageGenerator {
	
	private Image img = new Image("res/image.jpeg", 800, 800, true, false);
	private ImageView ImageView= new ImageView(img);
	Map<Integer, ImageView> tiles = new HashMap<>();
	
	
	public Map<Integer, ImageView> getTiles() {
		return tiles;
	}

	

	public void createImages(int numberOfTiles, Image img, int size ) {
		this.img = img;
		
		
		int numberOfRows = (int) Math.sqrt(numberOfTiles);
		int x=-size, y=0;
		
		for (int i = 0; i < numberOfTiles;i++) {
			if (i!=0&&(i%numberOfRows)==0) {
				y+=size;
				x=0;
			}
			else {
				x+=size;
			}
			
			Rectangle2D rect = new Rectangle2D(x, y, size, size);
			ImageView tmp = new ImageView(img);
			tmp.setViewport(rect);
			tiles.put(i+1, tmp);
		};
	}
	
	public String randomMove() {
		double tmp = Math.random();
		if (tmp<0.25) {
			return "UP";
		}
		else if (tmp<0.5) {
			return "RIGHT";
		}
		else if (tmp<0.75) {
			return "DOWN";
		}
		else return "LEFT";
	}
	
	public List<Tile> shuffle(int number, int diff){
		int place = number;
		int squirt = (int) Math.sqrt(number);
		List <Tile> finito = new ArrayList<>();
		Map<Integer, Tile> result = new HashMap<>();
		for (int i = 1; i <= number; i++) {
			
			Tile tmp = new Tile(i, i);
			result.put(i, tmp);
		}
		
		for (int i = 0; i < diff; i++) {
			boolean isItOk = true;
			String move = randomMove();
			switch (move) {
			case "UP":
				if (place-squirt>0) {
					System.out.println("UP"+ place);
					Tile blank = result.get(place);
					Tile newBlank = result.get(place-squirt);
					blank.setPlace(place-squirt);
					newBlank.setPlace(place);
					result.remove(place);
					result.remove(place-squirt);
					result.put(place, newBlank);
					result.put(place-squirt, blank);
					int tmpplace = place - squirt;
					place = tmpplace;
				}
				else i--;
				break;
			case "RIGHT":
				if((place-1+squirt)%squirt!=0) {
					System.out.println("right"+ place);
					Tile blank = result.get(place);
					Tile newBlank = result.get(place-1);
					blank.setPlace(place-1);
					newBlank.setPlace(place);
					result.remove(place);
					result.remove(place-1);
					result.put(place, newBlank);
					result.put(place-1, blank);
					int tmpplace = place - 1;
					place = tmpplace;
				}
				else i--;
				break;
				
			case "DOWN":
				if (place+squirt<=number) {
					System.out.println("down"+ place);
					Tile blank = result.get(place);
					Tile newBlank = result.get(place+squirt);
					blank.setPlace(place+squirt);
					newBlank.setPlace(place);
					result.remove(place);
					result.remove(place+squirt);
					result.put(place, newBlank);
					result.put(place+squirt, blank);
					int tmpplace = place + squirt;
					place = tmpplace;
				}
				else i--;
				break;
				
			case "LEFT":
				if(place%squirt!=0) {
					
					System.out.println("left"+ place);
					Tile blank = result.get(place);
					Tile newBlank = result.get(place+1);
					blank.setPlace(place+1);
					newBlank.setPlace(place);
					result.remove(place);
					result.remove(place+1);
					result.put(place, newBlank);
					result.put(place+1, blank);
					int tmpplace = place + 1;
					place = tmpplace;
				}
				else i--;
				break;
			default:
				i--;
				break;
			}	
		}
		
		for (int j = 1; j <= number; j++) {
			finito.add(result.get(j));
		}
		
		return finito;
	}

		
		
		
		
	

	

	
	
}
