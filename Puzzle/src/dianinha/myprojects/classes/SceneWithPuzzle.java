package dianinha.myprojects.classes;

import java.util.ArrayList;
import java.util.List;

import dianinha.myproject.enums.Difficulty;
import dianinha.myproject.enums.NumTiles;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class SceneWithPuzzle extends Scene {

	static BorderPane border = new BorderPane();
	static int numberOfMoves = 0;

	boolean checkPuzzle = false;
	int x, y, wholeSize, difficulty, numberOfTiles;
	int picSize = 0;
	Button check;
	HBox down;
	Image img;
	Difficulty choosenDiff;
	NumTiles choosenNumTiles;
	EventHandler<MouseEvent> UserClick;
	EventHandler<MouseEvent> CheckClick;
	Border choosenBorder = new Border(
			new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));

	public SceneWithPuzzle(int width, int height, Difficulty diff, NumTiles numTiles, String pathToImage) {
		super(border, width, height);

		FlowPane rootNode = new FlowPane(10, 10);
		border.setCenter(rootNode);
		down = new HBox();
		check = new Button("SPRAWDZ");

		if (numTiles == NumTiles.NINE) {
			numberOfTiles = 9;
			picSize = 220;
		} else if (numTiles == NumTiles.SIXTEEN) {
			numberOfTiles = 16;
			picSize = 160;
		} else {
			numberOfTiles = 25;
			picSize = 130;
		}

		if (diff == Difficulty.EASY) {
			//// changed for testing
			difficulty = 1;
		} else if (diff == Difficulty.NORMAL) {
			difficulty = 200;
		} else {
			difficulty = 300;
		}
		;

		// size of whole picture
		wholeSize = (int) (Math.sqrt(numberOfTiles) * picSize);

		img = new Image(pathToImage, wholeSize, wholeSize, true, false);
		ImageGenerator creatingTilesFromImages = new ImageGenerator();
		creatingTilesFromImages.createImages(numberOfTiles, img, picSize);

		List<ImageView> tmpListOfCreatedImageViews = new ArrayList<>();

		for (int i = 0; i < numberOfTiles; i++) {
			// blank last tile
			if (i + 1 == numberOfTiles) {
				tmpListOfCreatedImageViews
						.add(new ImageView(new Image("res/blank.png", picSize, picSize, true, false)));
			} else
				tmpListOfCreatedImageViews.add(creatingTilesFromImages.getTiles().get(i + 1));
		}

		List<Tile> shuffledListOfTiles = creatingTilesFromImages.shuffle(numberOfTiles, difficulty);

		List<ImageView> finalShuffeledListOfImageView = new ArrayList<>();
		List<Integer> orderOfTiles = new ArrayList<>();

		for (int i = 0; i < numberOfTiles; i++) {
			Tile tmp = shuffledListOfTiles.get(i);
			int whichImage = tmp.getImage();
			orderOfTiles.add(whichImage);
			finalShuffeledListOfImageView.add(tmpListOfCreatedImageViews.get(whichImage - 1));
		}

		List<Button> buttons = new ArrayList<>();
		for (int i = 0; i < numberOfTiles; i++) {
			buttons.add(new Button());
			buttons.get(i).setPrefSize(picSize, picSize);
			buttons.get(i).setGraphic(finalShuffeledListOfImageView.get(i));
			buttons.get(i).setId("" + orderOfTiles.get(i));

		}

		UserClick = new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// Button blank;
				int indexOfBlank = -10;
				int indexOfChosen = buttons.indexOf((Button) event.getSource()) + 1;

				for (Button button : buttons) {
					if (Integer.parseInt(button.getId()) == numberOfTiles) {
						// blank = button;
						indexOfBlank = buttons.indexOf(button) + 1;
					}

				}
				;

				if (Integer.parseInt(((Button) event.getSource()).getId()) == numberOfTiles) {
				} else {
					if (isItPossible(indexOfBlank, indexOfChosen)) {
						switchTiles((Button) event.getSource());
					}
				}

			}

			private void switchTiles(Button source) {
				numberOfMoves++;
				int indexChoosen = buttons.indexOf(source);
				int blankIndex = -100;

				Button switched = buttons.get(indexChoosen);
				Button blank = null;

				rootNode.getChildren().removeAll(buttons);
				for (Button button : buttons) {
					if (Integer.parseInt(button.getId()) == numberOfTiles) {
						blank = button;
						blankIndex = buttons.indexOf(button);
					}
				}
				for (int i = 0; i < buttons.size(); i++) {

					if (i == indexChoosen) {
						buttons.remove(indexChoosen);
						buttons.add(indexChoosen, blank);
					}
					if (i == blankIndex) {
						buttons.remove(blankIndex);
						buttons.add(blankIndex, switched);
					}
				}

				rootNode.getChildren().addAll(buttons);
			};
		};

		for (Button button : buttons) {
			button.setOnMouseClicked(UserClick);
		}
		rootNode.getChildren().addAll(buttons);

		CheckClick = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				for (int i = 1; i <= numberOfTiles; i++) {
					if (Integer.parseInt(buttons.get(i - 1).getId()) == i) {
						checkPuzzle = true;
					} else {
						checkPuzzle = false;
						break;
					}

				}
				;
				if (checkPuzzle) {
					check.setText("BRAWO! PRZEJDZ DO TABLICY WYNIK�W!");
				} else

				{
					check.setText("Niestety, probuj dalej!");
				}
			}
		};

		check.setOnMouseClicked(CheckClick);
		down.getChildren().add(check);
		down.setAlignment(Pos.CENTER);
		down.setPadding(new Insets(20));
		border.setBottom(down);

	}

	private boolean isItPossible(int blankIndex, int choiceIndex) {
		int tilesInRow = (int) Math.sqrt((numberOfTiles));

		boolean result = false;
		if (blankIndex - choiceIndex == tilesInRow || blankIndex - choiceIndex == -tilesInRow) {
			result = true;
		} else if (blankIndex - choiceIndex == 1 && choiceIndex % tilesInRow != 0) {
			result = true;
		} else if (blankIndex - choiceIndex == -1 && blankIndex % tilesInRow != 0) {
			result = true;
		} else
			result = false;
		return result;
	}

}
